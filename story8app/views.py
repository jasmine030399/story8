from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.http import HttpResponse
from .models import regis
from django.contrib.auth import logout as logout_user
from django.views.decorators.csrf import csrf_exempt
import requests
response = {}
response['counter'] = 0


# Create your views here.
def index(request):
     return render(request , 'index.html', {})

def profil(request):
     return render(request, 'index.html', {})

def buku(request):
     if request.user.is_authenticated:
          if 'counter' not in request.session :
               request.session['counter'] = 0
          response['counter'] = request.session['counter']
          if 'name' not in request.session :
               request.session['name'] = request.user.username
          if 'email' not in request.session :
               request.session['email'] = request.user.email
     return render(request, 'book.html', {})

def data(request):
     url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
     dts = requests.get(url).json()
     return JsonResponse(dts)

def registrasi(request):
    if(request.method == "POST"):
        data = dict(request.POST.items())
        tamu = regis(nama = data['name'],email = data['email'],password = data['password'])
        try:
            tamu.full_clean()
            tamu.save()
        except Exception as e:
            print(e.message_dict.items())
            e = [v[0] for k, v in e.message_dict.items()]
            return JsonResponse({"error": e}, status = 400)
        return JsonResponse({"message":"Data berhasil disimpan"})
    else:
        return render(request, 'registrasi.html', {})
def email_availability(request):
    email = request.GET.get("address")
    emails = regis.objects.filter(email=email).all()
    return JsonResponse({"email": email, "available": not bool(emails)})

def login(request):
    return render(request, 'login.html')

def logout(request):
    logout_user(request)
    return redirect('index')

def addCounter(request):
    if request.user.is_authenticated :
        request.session['counter'] = request.session['counter'] + 1
    else:
        pass
    return HttpResponse(request.session['counter'], content_type = 'application/json')
def substractCounter(request):
    if request.user.is_authenticated :
        request.session['counter'] = request.session['counter'] - 1
    else:
        pass
    return HttpResponse(request.session['counter'], content_type = 'application/json')




