from django.conf.urls import url
from .views import index
from .views import profil
from .views import buku
from .views import data
from .views import registrasi
from .views import email_availability
from .views import login
from .views import logout
from .views import substractCounter
from .views import addCounter

urlpatterns = [
 url(r'^$', index, name ='index'),
 url(r'^profil/', profil, name ='profil'),
 url(r'^book/', buku, name='buku'),
 url(r'^data/', data, name='data'),
 url(r'^registrasi/', registrasi, name='registrasi'),
 url(r'^email-availability', email_availability, name='email-availability'),
 url(r'^login/', login, name='login'),
 url(r'^logout/', logout, name='logout'),
 url(r'substract', substractCounter, name = 'substract'),
 url(r'add', addCounter, name = 'add'),

 
]
