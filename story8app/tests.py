from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import email_availability

from .views import registrasi

# Create your tests here.
class story8UnitTest(TestCase):
    def test_story_8_url_is_exist(self):
        response = Client().get('/#/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landing_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Hello',html_response)

    def test_landing_contains_text(self):
        response = Client().get('/')
        self.assertContains(response, 'Aktivitas')
        self.assertContains(response, 'Kepanitiaan/Organisasi')
        self.assertContains(response, 'Prestasi')

    def test_story9_url_exists(self):
        response = Client.get('/profil/')
        self.assertEqual(response.status_code, 200)

    def test_story9_url_exists(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_story9_uses_book_as_template(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'book.html')

    def test_story10_registrasi_url_exists(self):
        response = Client().get('/registrasi/')
        self.assertEqual(response.status_code, 200)
    
    def test_story10_availability_url_exists(self):
        response = Client().get('/email-availability/?address=testtest@gmail.com/')
        self.assertEqual(response.status_code, 200)

    # def test_story10_post_success_and_render_the_result(self):
    #     test = 'Anonymous'
    #     response_post = Client().post('/registrasi', {'name' : 'test', 'email' : 'testtest@gmail.com', 'password' : 'hahaha'})
    #     self.assertEqual(response_post.status_code, 200)
	
    def test_story10_post_fail_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/registrasi', {'name' : '', 'email' : '', 'password' : ''})
        self.assertEqual(response_post.status_code, 404)

   


    
